$(document).ready(function(){
    "use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */


//Tạo biến global các size 
const gSIZES = "SIZE S";
const gSIZEM = "SIZE M";
const gSIZEL = "SIZE L";

//Tạo biến global cho các loại pizza
const gPIZZAOCEAN = "PIZZA OCEAN";
const gPIZZAHAWAI = "PIZZA HAWAI";
const gPIZZABACON = "PIZZA BACON"

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
//Tạo sự kiện tải trang 
onPageLoading();

//Gán sự kiện click cho nút gửi 
$("#btn-send").on("click",function(){
    onBtnSendClick();
})
//Gán sự kiện click cho các nút Chọn (Chọn size Pizza)
$("#btn-order-size-s").on("click",function(){
    onBtnSizeSClick();
});
$("#btn-order-size-m").on("click",function(){
    onBtnSizeMClick();
});
$("#btn-order-size-l").on("click",function(){
    onBtnSizeLClick();
});

//Gán sự kiện cho các nút Chọn (Chọn loại pizza)
$("#btn-pizza-ocean").on("click",function(){
    onBtnPizzaOceanClick();
});
$("#btn-pizza-hawai").on("click",function(){
    onBtnPizzaHawaiClick();
});
$("#btn-pizza-bacon").on("click",function(){
    onBtnPizzaBaconClick();
});

//Gán sự kiện cho nút quay lại
$("#btn-back").on("click",function(){
    onBtnBackClick();
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */ 
//Hàm xử lý sự kiện tải trang
function onPageLoading(){
    callAjaxGetDrink();
}

//Hàm xử lý khi click vào nút gửi 
function onBtnSendClick(){
    var vDonHang = {
        menuDuocChon: null,     
        loaiPizza: null,
        loaiNuocUong: "",
        idLoaiNuocUong : "",
        hoVaTen: "", 
        email: "",
        dienThoai: "",
        diaChi: "",
        loiNhan: "",
        voucher: "",
        phanTramGiamGia: 0,
        // method tính giá phải thanh toán 
        priceActualVND : function (){
              return this.menuDuocChon.price * (1-this.phanTramGiamGia/100 );
          }
      }
      //B1 : Thu thập dữ liệu trên form
      thuThapDuLieu(vDonHang);
      //B2 : Kiểm tra Dữ Liệu 
      var vDuLieuHople = kiemTraDuLieu(vDonHang);
      if(vDuLieuHople == true){
        console.log(vDonHang);
        //Đổ dữ liệu vào các trường input trên modal 
        handlerDataToModalOrderDetail(vDonHang);
        //Hiển thị modal
        $("#order-detail-modal").modal("show");
        //gán sự kiện click cho nút tạo đơn
        $("#btn-create-order").on("click",function(){
            onBtnCreateOrderClick(vDonHang);
        })
      }
}
//hàm xử lý khi click vào nút Tạo Đơn 
function onBtnCreateOrderClick(paramDonHangObj){
    // ẩn modal order detail
    $("#order-detail-modal").modal("hide");
    //B1 : tạo một object và thu thập dữ liệu

    var vObjectRequest = {
        kichCo: paramDonHangObj.menuDuocChon.size,
        duongKinh: paramDonHangObj.menuDuocChon.duongKinh,
        suon: paramDonHangObj.menuDuocChon.suongNuong,
        salad: paramDonHangObj.menuDuocChon.salad,
        loaiPizza: paramDonHangObj.loaiPizza,
        idVourcher: paramDonHangObj.voucher,
        idLoaiNuocUong: paramDonHangObj.idLoaiNuocUong,
        soLuongNuoc: paramDonHangObj.menuDuocChon.nuocNgot,
        hoTen: paramDonHangObj.hoVaTen,
        thanhTien: paramDonHangObj.priceActualVND(),
        email: paramDonHangObj.email,
        soDienThoai: paramDonHangObj.dienThoai,
        diaChi: paramDonHangObj.diaChi,
        loiNhan: paramDonHangObj.loiNhan
    }

    //B2 : Kiểm Tra - Bỏ qua

    //B3 : call Ajax to create New Order
    callAjaxCreateNewOrder(vObjectRequest);

    //hiển thị modal result
    $("#result-modal").modal("show");
}

//Hàm xử lý call ajax to create new order
 function callAjaxCreateNewOrder(paramObjectRequest){
    $.ajax({
        url : "http://203.171.20.210:8080/devcamp-pizza365/orders",
        type : 'POST',
        contentType : 'application/json;charset=UTF-8',
        data :JSON.stringify(paramObjectRequest),
        success : function(res){
            console.log(res);
            var vOrderCode = res.orderCode;
            handlerDataToModalResultInp(vOrderCode);
            alert("Xin chúc mừng , bạn đã tạo đơn thành công ");
            window.location.href = "Pizza365-v1.9.2.html"
            
        },
        error : function(err){
            console.log(err.responseText); 
        }

    })
}

//Hàm reset lại form modal detail khi click vào nút quay lại
function onBtnBackClick(){
    $("#inp-modal-name").val("");
    $("#inp-modal-phone-number").val("");
    $("#inp-modal-address").val("");
    $("#inp-modal-message").val("");
    $("#inp-modal-voucher").val("");
}
//Hàm xử lý đổ dữ liệu order code vào input của modal result
function handlerDataToModalResultInp(paramOrderCode){
    $("#inp-code-create-order").val(paramOrderCode);
}
//Hàm xử lý khi click vào nút chọn pizza Ocean
function onBtnPizzaOceanClick(){
    console.log("%cBạn chọn " + gPIZZAOCEAN,"color:blue;");
    changeColorTypeButton(gPIZZAOCEAN);
}
//Hàm xử lý khi click vào nút chọn pizza Ocean
function onBtnPizzaHawaiClick(){
    console.log("%cBạn chọn " + gPIZZAHAWAI,"color:blue;");
    changeColorTypeButton(gPIZZAHAWAI);
}
//Hàm xử lý khi click vào nút chọn pizza Ocean
function onBtnPizzaBaconClick(){
    console.log("%cBạn chọn " + gPIZZABACON,"color:blue;");
    changeColorTypeButton(gPIZZABACON);
    
}

//Hàm sử lý khi click vào nút Chọn size S
function onBtnSizeSClick(){
    console.log("%cBạn chọn size S","color:red;");
    changeColorSizeButton(gSIZES);
    var vSelectPizzaSize = getPizzaSize(gSIZES,"20cm", 2, "200g", 2, 150000);
    vSelectPizzaSize.displayInConsoleLog();
} 
//Hàm xử lý khi click vào nút chọn size M
function onBtnSizeMClick(){
    console.log("%cBạn chọn size M","color:red;");
    changeColorSizeButton(gSIZEM);
    var vSelectPizzaSize = getPizzaSize(gSIZEM,"25cm", 4, "300g", 3, 200000);
    vSelectPizzaSize.displayInConsoleLog();
}
//Hàm xử lý khi click vào nút chọn size L
function onBtnSizeLClick(){
    console.log("%cBạn chọn size L","color:red;");
    changeColorSizeButton(gSIZEL);
    var vSelectPizzaSize = getPizzaSize(gSIZEL,"30cm", 8, "500g", 4, 250000);
    vSelectPizzaSize.displayInConsoleLog();
}

;
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm đổ dữ liệu vào modal order detail
function handlerDataToModalOrderDetail(paramDonHangObj){
    $("#inp-modal-name").val(paramDonHangObj.hoVaTen);
    $("#inp-modal-phone-number").val(paramDonHangObj.dienThoai);
    $("#inp-modal-address").val(paramDonHangObj.diaChi);
    $("#inp-modal-message").val(paramDonHangObj.loiNhan);
    $("#inp-modal-voucher").val(paramDonHangObj.voucher);

    var vDetailText = 
    `
    Xác nhận : ${paramDonHangObj.hoVaTen} , ${paramDonHangObj.dienThoai} , ${paramDonHangObj.diaChi}  
    Menu : ${paramDonHangObj.menuDuocChon.size} , sườn nướng : ${paramDonHangObj.menuDuocChon.suongNuong} , nước : ${paramDonHangObj.menuDuocChon.nuocNgot}... 
    Loại pizza : ${paramDonHangObj.loaiPizza} , Giá : ${paramDonHangObj.menuDuocChon.price} vnd, Mã giảm giá : ${paramDonHangObj.voucher} 
    Phải thanh toán : ${paramDonHangObj.priceActualVND()} vnd (giảm giá ${paramDonHangObj.phanTramGiamGia}%)
    `;
    $("#inp-modal-detail").val(vDetailText);
}

//Hàm thu thập dữ liệu trên website 
function thuThapDuLieu(paramDonHangObj){
    //Thu Thập Menu 
    var vSizeSSelected = $("#btn-order-size-s").attr("data-is-selected-size");
    var vSizeMSelected = $("#btn-order-size-m").attr("data-is-selected-size");
    var vSizeLSelected = $("#btn-order-size-l").attr("data-is-selected-size");

    if(vSizeSSelected === "Y"){
        paramDonHangObj.menuDuocChon = getPizzaSize(gSIZES,"20cm", 2, "200g", 2, 150000) ;
    }
    else if(vSizeMSelected === "Y"){
        paramDonHangObj.menuDuocChon = getPizzaSize(gSIZEM,"25cm", 4, "300g", 3, 200000);
    }
    else if(vSizeLSelected === "Y"){
        paramDonHangObj.menuDuocChon = getPizzaSize(gSIZEL,"30cm", 8, "500g", 4, 250000) ;
    }

    //Thu Thập Loại Pizza
    // var vTypePizzaSelected = null ;
    var vPizzaOceanSelected = $("#btn-pizza-ocean").attr("data-is-selected-pizza");
    var vPizzaHwaiSelected = $("#btn-pizza-hawai").attr("data-is-selected-pizza");
    var vPizzaBaconSelected = $("#btn-pizza-bacon").attr("data-is-selected-pizza");

    if(vPizzaOceanSelected === "Y"){
        // vTypePizzaSelected = gPIZZAOCEAN;
        paramDonHangObj.loaiPizza = gPIZZAOCEAN;
    }
    else if(vPizzaHwaiSelected === "Y"){
        // vTypePizzaSelected = gPIZZAHAWAI;
        paramDonHangObj.loaiPizza = gPIZZAHAWAI;
    }
    else if(vPizzaBaconSelected === "Y"){
        // vTypePizzaSelected = gPIZZABACON;
        paramDonHangObj.loaiPizza = gPIZZABACON;
    }

    paramDonHangObj.loaiNuocUong = $("#select-drink option:selected").text();
    paramDonHangObj.idLoaiNuocUong = $("#select-drink").val();
    paramDonHangObj.hoVaTen = $("#inp-fullname").val();
    paramDonHangObj.email = $("#inp-email").val();
    paramDonHangObj.dienThoai = $("#inp-dien-thoai").val();
    paramDonHangObj.diaChi = $("#inp-dia-chi").val();
    paramDonHangObj.loiNhan =$("#inp-message").val();
    paramDonHangObj.voucher = $("#inp-ma-giam-gia").val();

    if(paramDonHangObj.voucher == ""){
        paramDonHangObj.phanTramGiamGia = 0;
    }
    else {
        paramDonHangObj.phanTramGiamGia = getAjaxPhanTramGiamGia(paramDonHangObj.voucher);
    }
}
//Hàm call api ajax lấy phần trăm giảm giá 
function getAjaxPhanTramGiamGia(paramVoucher){
    var vPhanTramGiamGia = 0;
    $.ajax({
        url : "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail" + "/" + paramVoucher,
        type : 'GET',
        dataType : 'json',
        async :false,
        success : function(responseObj){
            console.log(responseObj);
            vPhanTramGiamGia = responseObj.phanTramGiamGia;
        },
        error : function(err){
            console.log(err.responseTexts);
        }
    })
    return vPhanTramGiamGia;
}

//Hàm kiểm tra dữ liệu trên form
function kiemTraDuLieu(paramDonHangObj){
    if(paramDonHangObj.menuDuocChon == null){
        alert("Bạn chưa chọn Menu Pizza !");
        return false;
    }

    if(paramDonHangObj.loaiPizza == null){
        alert("Bạn chưa chọn Loại Pizza !");
    }

    if(paramDonHangObj.loaiNuocUong == "Tất cả các loại nước uống"){
        alert("Bạn hãy chọn một đồ uống bất kỳ");
        return false;
    }

    if(paramDonHangObj.hoVaTen == ""){
        alert("Hãy nhập họ và tên");
        return false;
    }

    var vCheckEmail = kiemTraEmail(paramDonHangObj.email);
    if(vCheckEmail == false){
        return false;
    }

    if(paramDonHangObj.dienThoai == ""){
        alert("Bạn chưa nhập số điện thoại");
        return false;
    }

    if(isNaN(paramDonHangObj.dienThoai)){
        alert("Điện thoại phải là số ");
        return false;
    }

    if(paramDonHangObj.diaChi == ""){
        alert(" Hãy nhập địa chỉ để thuận tiện cho việc giao hàng");
        return false;
    }

    return true;
    
}

//Hàm kiểm tra email
function kiemTraEmail(paramEmail){
    var vEmail = paramEmail;
    // nếu email nhập là rỗng 
    if(vEmail == ""){
      alert("Bạn phải nhập Email !");
      return false;
    }

    // Nếu email nhập không chứa ký tự @ 
    if(vEmail.includes("@") == false){
      alert("Email phải chưa ký tự @");
      return false;
    }

    // Kiểm tra trước và sau ký tự @ có ký tự nào hay không
    if(vEmail.startsWith("@") == true || vEmail.endsWith("@") == true){
      alert("Email không được bắt đầu và kết thúc bằng ký tư '@' ");
      return false;
    }
    return true;
}
//Hàm call Api Ajax lấy dữ liệu đồ uống về 
function callAjaxGetDrink(){
    $.ajax({
        url : "http://203.171.20.210:8080/devcamp-pizza365/drinks",
        type : 'GET',
        dataType : 'json',
        success : function(responseObj){
            handlerDataToSelectDrink(responseObj);
            console.log(responseObj);
        },
        error : function(error){
            console.log(error.responseText);
        }
    })
}
//Hàm xử lý đổ dữ liệu vào ô select chọn đồ uống 
function handlerDataToSelectDrink(paramResponse){
    for (var bI = 0 ; bI < paramResponse.length ; bI ++){
        $("<option>",{
            value : paramResponse[bI].maNuocUong,
            text : paramResponse[bI].tenNuocUong
        })
        .appendTo($("#select-drink"));
    }
}
//Hàm xử lý đổi màu nút chọn cho các nút chọn Loại Pizza
function changeColorTypeButton(paramTypePizza){
    if(paramTypePizza === gPIZZAOCEAN ){
        $("#btn-pizza-ocean")
        .removeClass("orange")
        .addClass("btn-success")
        .attr("data-is-selected-pizza","Y");

        $("#btn-pizza-hawai")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-pizza","N");

        $("#btn-pizza-bacon")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-pizza","N");
    }
    else if(paramTypePizza === gPIZZAHAWAI){
        $("#btn-pizza-ocean")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-pizza","N");

        $("#btn-pizza-hawai")
        .removeClass("orange")
        .addClass("btn-success")
        .attr("data-is-selected-pizza","Y");

        $("#btn-pizza-bacon")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-pizza","N");
    }
    else if(paramTypePizza === gPIZZABACON){
        $("#btn-pizza-ocean")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-pizza","N");

        $("#btn-pizza-hawai")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-pizza","N");

        $("#btn-pizza-bacon")
        .removeClass("orange")
        .addClass("btn-success")
        .attr("data-is-selected-pizza","Y");
    }
}
//Hàm xử lý đổi màu nút cho các nút chọn size 
function changeColorSizeButton(paramSize){
    if(paramSize === gSIZES ){
        $("#btn-order-size-s")
        .removeClass("orange")
        .addClass("btn-success")
        .attr("data-is-selected-size","Y");

        $("#btn-order-size-m")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-size","N");

        $("#btn-order-size-l")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-size","N");
    }
    else if(paramSize === gSIZEM){
        $("#btn-order-size-s")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-size","N");

        $("#btn-order-size-m")
        .removeClass("orange")
        .addClass("btn-success")
        .attr("data-is-selected-size","Y");

        $("#btn-order-size-l")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-size","N");
    }
    else if(paramSize === gSIZEL){
        $("#btn-order-size-s")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-size","N");

        $("#btn-order-size-m")
        .removeClass("btn-success")
        .addClass("orange")
        .attr("data-is-selected-size","N");

        $("#btn-order-size-l")
        .removeClass("orange")
        .addClass("btn-success")
        .attr("data-is-selected-size","Y");
    }
}
//Hàm xử lý lấy thông tin menu size và trả về một object
function getPizzaSize(paramSize,paramDuongKinh,paramSuonNuong,paramSalad,paramNuocNgot,paramPrice){
    var vSelectedPizzaSize = {
      size : paramSize,
      duongKinh : paramDuongKinh,
      suongNuong : paramSuonNuong,
      salad: paramSalad,
      nuocNgot : paramNuocNgot,
      price: paramPrice,

      //method 
      //method display pizza Size Infor
      displayInConsoleLog(){
        // console.log("%c Pizza Size Selected - Size Pizza được chọn ......","color:red;");
        console.log(this.size);
        console.log("Đường kính : " + this.duongKinh);
        console.log("Sường nướng : " + this.suongNuong);
        console.log("Salad Gram : " + this.salad);
        console.log("Nước Ngọt : " + this.nuocNgot);
        console.log("Giá : " + this.price);
      }
    }
    // trả lại 01 đối tượng, có đủ thuộc tính và các phương thức (methods)
    return vSelectedPizzaSize;
}
});
